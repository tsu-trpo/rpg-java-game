package com.justgame.UI;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.utils.Align;
import com.justgame.ServiceLocator;
import com.justgame.managers.TexturesAssetManager;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class StatusUI extends Window implements PropertyChangeListener {
	private Image hpBar;
	private Image mpBar;
	private Image xpBar;
	private Image staBar;

	private ImageButton inventoryButton;
	private ImageButton skillButton;

	//Attributes
	private int levelVal = 1;
	private int goldVal = 0;
	private int hpVal = 100;
	private int mpVal = 100;
	private int xpVal = 1;
	private int staVal = 100;

	private int xpMax = 999;
	private int hpMax = 999;
	private int mpMax = 999;

	private Label hpValLabel;
	private Label mpValLabel;
	private Label xpValLabel;
	private Label levelValLabel;
	private Label goldValLabel;
	private Label staValLabel;

	private float barWidth = 0;
	private float barHeight = 0;

	private TexturesAssetManager textureManager = ServiceLocator.getTextureManager();

	public StatusUI() {
		super("stats", ServiceLocator.getTextureManager().getSkin(TexturesAssetManager.STATUSUI_JSON));
		Skin statusSkin = textureManager.getSkin(textureManager.STATUSUI_JSON);

		//groups
		WidgetGroup hpGroup = new WidgetGroup();
		WidgetGroup mpGroup = new WidgetGroup();
		WidgetGroup xpGroup = new WidgetGroup();
		WidgetGroup staGroup = new WidgetGroup();

		//images
		hpBar = new Image(statusSkin.getDrawable("HP_Bar"));
		Image hpBarFrame = new Image(statusSkin.getDrawable("Bar"));
		mpBar = new Image(statusSkin.getDrawable("MP_Bar"));
		Image mpBarFrame = new Image(statusSkin.getDrawable("Bar"));
		xpBar = new Image(statusSkin.getDrawable("XP_Bar"));
		Image xpBarFrame = new Image(statusSkin.getDrawable("Bar"));

		staBar = new Image(statusSkin.getDrawable("XP_Bar"));
		Image staBarFrame = new Image(statusSkin.getDrawable("Bar"));

		barWidth = hpBar.getWidth();
		barHeight = hpBar.getHeight();

		//labels
		Label hpLabel = new Label(" hp: ", statusSkin);
		hpValLabel = new Label(String.valueOf(hpVal), statusSkin);
		Label mpLabel = new Label(" mp: ", statusSkin);
		mpValLabel = new Label(String.valueOf(mpVal), statusSkin);
		Label xpLabel = new Label(" xp: ", statusSkin);
		xpValLabel = new Label(String.valueOf(xpVal), statusSkin);
		Label levelLabel = new Label(" lvl: ", statusSkin);
		levelValLabel = new Label(String.valueOf(levelVal), statusSkin);
		Label goldLabel = new Label(" gold; ", statusSkin);
		goldValLabel = new Label(String.valueOf(goldVal), statusSkin);

		Label staLabel = new Label(" st: ", statusSkin);
		staValLabel = new Label(String.valueOf(staVal), statusSkin);

		//buttons
		inventoryButton = new ImageButton(statusSkin, "inventory-button");
		inventoryButton.getImageCell().size(32, 32);

		skillButton = new ImageButton(statusSkin, "skill-button");
		skillButton.getImageCell().size(32, 32);

		//Align images
		hpBar.setPosition(3, 6);
		mpBar.setPosition(3, 6);
		xpBar.setPosition(3, 6);
		staBar.setPosition(3, 6);

		//add to widget groups
		hpGroup.addActor(hpBarFrame);
		hpGroup.addActor(hpBar);
		mpGroup.addActor(mpBarFrame);
		mpGroup.addActor(mpBar);
		xpGroup.addActor(xpBarFrame);
		xpGroup.addActor(xpBar);
		staGroup.addActor(staBarFrame);
		staGroup.addActor(staBar);

		//Add to layout
		defaults().expand().fill();

		//account for the title padding
		this.pad(this.getPadTop() + 10, 10, 10, 10);

		this.add();
		this.add(skillButton).align(Align.center);
		this.add(inventoryButton).align(Align.right);
		this.row();

		this.add(hpGroup).size(hpBarFrame.getWidth(), hpBarFrame.getHeight()).padRight(10);
		this.add(hpLabel);
		this.add(hpValLabel).align(Align.left);
		this.row();

		this.add(mpGroup).size(mpBarFrame.getWidth(), mpBarFrame.getHeight()).padRight(10);
		this.add(mpLabel);
		this.add(mpValLabel).align(Align.left);
		this.row();

		this.add(xpGroup).size(xpBarFrame.getWidth(), xpBarFrame.getHeight()).padRight(10);
		this.add(xpLabel);
		this.add(xpValLabel).align(Align.left).padRight(20);
		this.row();

		this.add(staGroup).size(staBarFrame.getWidth(), staBarFrame.getHeight()).padRight(10);
		this.add(staLabel);
		this.add(staValLabel).align(Align.left).padRight(20);
		this.row();

		this.add(levelLabel).align(Align.left);
		this.add(levelValLabel).align(Align.left);
		this.row();
		this.add(goldLabel);
		this.add(goldValLabel).align(Align.left);

		this.pack();
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if ("health".equals(evt.getPropertyName())) {
			hpValLabel.setText(evt.getNewValue().toString());
		} else if ("mana".equals(evt.getPropertyName())) {
			mpValLabel.setText(evt.getNewValue().toString());
		} else if ("experience".equals(evt.getPropertyName())) {
			xpValLabel.setText(evt.getNewValue().toString());
		} else if ("stamina".equals(evt.getPropertyName())) {
			staValLabel.setText(evt.getNewValue().toString());
		}
	}

	public ImageButton getInventoryButton() {
		return inventoryButton;
	}

	public ImageButton getSkillButton() {
		return skillButton;
	}

	public int getLevelValue() {
		return levelVal;
	}

	public int getXPValue() {
		return xpVal;
	}

	public int getXPValueMax() {
		return xpMax;
	}

	public void setXPValueMax(int maxXPValue) {
		this.xpMax = maxXPValue;
	}

	//HP
	public int getHPValue() {
		return hpVal;
	}

	public int getHPValueMax() {
		return hpMax;
	}

	public void setHPValueMax(int maxHPValue) {
		this.hpMax = maxHPValue;
	}

	//MP
	public int getMPValue() {
		return mpVal;
	}

	public int getMPValueMax() {
		return mpMax;
	}

	public void setMPValueMax(int maxMPValue) {
		this.mpMax = maxMPValue;
	}

	public void updateBar(Image bar, int currentVal, int maxVal) {
		int val = MathUtils.clamp(currentVal, 0, maxVal);
		float tempPercent = (float) val / (float) maxVal;
		float percentage = MathUtils.clamp(tempPercent, 0, 100);
		bar.setSize(barWidth * percentage, barHeight);
	}

}
