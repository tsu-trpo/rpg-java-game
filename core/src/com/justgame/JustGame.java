package com.justgame;

import com.badlogic.gdx.Game;
import com.justgame.managers.MusicAssetManager;
import com.justgame.managers.ScreenManager;
import com.justgame.managers.TexturesAssetManager;
import com.justgame.views.ViewScreen;

public class JustGame extends Game {
	@Override
	public void create() {
		ServiceLocator.setTextureManager(new TexturesAssetManager());
		ServiceLocator.setMusicManager(new MusicAssetManager());
		ServiceLocator.setScreenManager(new ScreenManager(this));
		ServiceLocator.setPreferences(new AppPreferences());
		ServiceLocator.getScreenManager().changeScreen(ViewScreen.LOADING);
	}

	@Override
	public void dispose() {
		ServiceLocator.getMusicManager().dispose();
		ServiceLocator.getTextureManager().dispose();
	}
}
