package com.justgame.managers;

import com.justgame.JustGame;
import com.justgame.views.*;

public class ScreenManager {
	private LoadingScreen loadingScreen;
	private PreferencesScreen preferencesScreen;
	private MenuScreen menuScreen;
	private GameScreen gameScreen;
	private StubScreen endScreen;
	private JustGame parent;

	public ScreenManager(JustGame justGame) {
		parent = justGame;
	}

	// Отложенная инициализация используется из-за того, что в момент вызова конструктора ScreenManager
	// класс ServiceLocator проинициализирован не польностью и не готов к работе.
	private void lazyInit() {
		if (loadingScreen == null) {
			loadingScreen = new LoadingScreen();
			menuScreen = new MenuScreen();
			preferencesScreen = new PreferencesScreen();
			gameScreen = new GameScreen();
			endScreen = new StubScreen();
		}
	}

	public void changeScreen(ViewScreen screen) {
		lazyInit();
		switch (screen) {
			case LOADING:
				parent.setScreen(loadingScreen);
				break;
			case MENU:
				parent.setScreen(menuScreen);
				break;
			case PREFERENCES:
				parent.setScreen(preferencesScreen);
				break;
			case GAME:
				parent.setScreen(gameScreen);
				break;
			case ENDGAME:
				parent.setScreen(endScreen);
				break;
		}
	}
}
