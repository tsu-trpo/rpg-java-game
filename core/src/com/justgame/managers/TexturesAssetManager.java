package com.justgame.managers;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.SkinLoader.SkinParameter;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class TexturesAssetManager {
	public static final String SKIN_JSON = "skin/uiskin.json";
	public static final String SKIN_ATLAS = "skin/uiskin.atlas";

	public static final String STATUSUI_JSON = "skin/statusui.json";
	public static final String STATUSUI_ATLAS = "skin/statusui.atlas";

	public static final String MAP = "maps/test_map.tmx";

	private final AssetManager manager = new AssetManager();

	public void loadSkins() {
		SkinParameter skin_params = new SkinParameter(SKIN_ATLAS);
		manager.load(SKIN_JSON, Skin.class, skin_params);

		SkinParameter status_params = new SkinParameter(STATUSUI_ATLAS);
		manager.load(STATUSUI_JSON, Skin.class, status_params);
	}

	public void loadMaps() {
		manager.setLoader(TiledMap.class, new TmxMapLoader(new InternalFileHandleResolver()));
		manager.load(MAP, TiledMap.class);
	}

	public Skin getSkin(String skin) {
		return manager.get(skin);
	}

	public TiledMap getMap(String map) {
		return manager.get(map);
	}

	public void finishLoading() {
		manager.finishLoading();
	}

	public void dispose() {
		manager.dispose();
	}
}
