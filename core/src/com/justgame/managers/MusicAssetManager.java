package com.justgame.managers;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.justgame.ServiceLocator;

public class MusicAssetManager {
	public static String menuSong = "music/MenuSong.mp3";
	public static String caveSong = "music/CaveSong.mp3";
	private final AssetManager manager = new AssetManager();
	private Music currentMusic;

	public void loadMusics() {
		manager.load(menuSong, Music.class);
		manager.load(caveSong, Music.class);
	}

	public void playMusic(String song) {
		disposeMusic();
		if (ServiceLocator.getPreferences().isMusicEnabled()) {
			currentMusic = manager.get(song);
			currentMusic.play();
			currentMusic.setVolume(ServiceLocator.getPreferences().getMusicVolume());
		}
	}

	private void disposeMusic() {
		if (currentMusic != null) {
			currentMusic.dispose();
		}
	}

	public void finishLoading() {
		manager.finishLoading();
	}

	public void dispose() {
		manager.dispose();
		disposeMusic();
	}

}
