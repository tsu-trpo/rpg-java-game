package com.justgame.map;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector3;
import com.justgame.camera.Camera;

public class MapInputController implements InputProcessor {
	private final float ZOOM_MULTIPLIER = 0.1f;
	private Camera camera;
	private Vector3 mousePosition = new Vector3();
	private float scrollMultiplier = 1;

	public MapInputController(Camera camera) {
		this.camera = camera;
	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		mousePosition.set(screenX, screenY, 0);

		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		Vector3 cameraPosition = new Vector3();

		cameraPosition.set((mousePosition.x - screenX) * scrollMultiplier,
		                   (screenY - mousePosition.y) * scrollMultiplier, 0);
		camera.translateCameraPosition(cameraPosition);

		mousePosition.set(screenX, screenY, 0);

		return true;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		if (!camera.changeCameraZoom(amount * ZOOM_MULTIPLIER)) {
			return false;
		}

		scrollMultiplier += amount * ZOOM_MULTIPLIER;

		return true;
	}
}
