package com.justgame.views;

import com.badlogic.gdx.Screen;
import com.justgame.ServiceLocator;

public class LoadingScreen implements Screen {
	public LoadingScreen() {
	}

	@Override
	public void show() {
		ServiceLocator.getTextureManager().loadSkins();
		ServiceLocator.getTextureManager().loadMaps();
		ServiceLocator.getTextureManager().finishLoading();
		ServiceLocator.getMusicManager().loadMusics();
		ServiceLocator.getMusicManager().finishLoading();
	}

	@Override
	public void render(float delta) {
		ServiceLocator.getScreenManager().changeScreen(ViewScreen.MENU);
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}
}
