package com.justgame.gamecomponentlogic.components;

import com.badlogic.ashley.core.Component;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class StatsComponent implements Component {
	private Integer health = 1;
	private Integer mana = 1;
	private Integer experience = 1;
	private Integer stamina = 1;

	private PropertyChangeSupport support;

	public StatsComponent() {
		support = new PropertyChangeSupport(this);
	}

	public static StatsComponent create() {
		return new StatsComponent();
	}

	public void addPropertyChangeListener(PropertyChangeListener pcl) {
		support.addPropertyChangeListener(pcl);
	}

	public void removePropertyChangeListener(PropertyChangeListener pcl) {
		support.removePropertyChangeListener(pcl);
	}

	public Integer getHealth() {
		return this.health;
	}

	public void setHealth(Integer health) {
		support.firePropertyChange("health", this.health, health);
		this.health = health;
	}

	public Integer getMana() {
		return this.mana;
	}

	public void setMana(Integer mana) {
		support.firePropertyChange("mana", this.mana, mana);
		this.mana = mana;
	}

	public Integer getExperience() {
		return this.experience;
	}

	public void setExperience(Integer expirience) {
		support.firePropertyChange("experience", this.experience, expirience);
		this.experience = expirience;
	}

	public Integer getStamina() {
		return this.stamina;
	}

	public void setStamina(Integer stamina) {
		support.firePropertyChange("stamina", this.stamina, stamina);
		this.stamina = stamina;
	}
}
