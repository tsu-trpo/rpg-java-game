package com.justgame.gamecomponentlogic.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class TransformComponent implements Component {
	public final Vector3 position = Vector3.Zero;
	public final Vector2 scale = new Vector2(1.0f, 1.0f);
	public float velocity = 100.0f;
	public float rotation = 0.0f;
	public boolean isHidden = false;

	public static TransformComponent create() {
		return new TransformComponent();
	}

	public TransformComponent setPosition(Vector2 position) {
		return setPosition(new Vector3(position.x, position.y, this.position.z));
	}

	public TransformComponent setPosition(Vector3 position) {
		this.position.set(position);
		return this;
	}

	public TransformComponent setScale(Vector2 scale) {
		this.scale.set(scale);
		return this;
	}

	public TransformComponent setVelocity(float velocity) {
		this.velocity = velocity;
		return this;
	}

	public TransformComponent setRotation(float rotation) {
		this.rotation = rotation;
		return this;
	}

	public TransformComponent hide() {
		this.isHidden = true;
		return this;
	}

	public TransformComponent show() {
		this.isHidden = false;
		return this;
	}

	public TransformComponent setHidden(boolean isHidden) {
		this.isHidden = isHidden;
		return this;
	}
}
