package com.justgame.gamecomponentlogic.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.justgame.gamecomponentlogic.Animations;

public class AnimationComponent implements Component {
	public ArrayMap<String, Animation> animations = new ArrayMap<String, Animation>();
	public float time = 0.0f;
	public boolean isLooping = false;
	private String state;

	public static AnimationComponent create() {
		return new AnimationComponent();
	}

	public void addAnimation(Animations.AnimationNode animation) {
		animations.put(animation.state,
		               new Animation<TextureRegion>(
				               animation.frameDuration,
				               createTextureRegionFromPng(animation.texturePath, animation.rows, animation.cols),
				               animation.mode
		               ));
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		if (this.state == state) {
			return;
		}

		this.state = state;
		time = 0.0f;
	}

	private Array<TextureRegion> createTextureRegionFromPng(String path, int rows, int cols) {
		Texture raw = new Texture(path);

		TextureRegion[][] tmp = TextureRegion
				.split(raw, raw.getWidth() / cols, raw.getHeight() / rows);

		Array<TextureRegion> frames = new Array<TextureRegion>();
		for (int i = 0; i < rows; ++i) {
			for (int j = 0; j < cols; ++j) {
				frames.add(tmp[i][j]);
			}
		}

		return frames;
	}
}
