package com.justgame.gamecomponentlogic.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.justgame.gamecomponentlogic.components.TransformComponent;

import java.util.Comparator;

public class ZComparator implements Comparator<Entity> {
	private ComponentMapper<TransformComponent> cmTrans;

	public ZComparator() {
		cmTrans = ComponentMapper.getFor(TransformComponent.class);
	}

	@Override
	public int compare(Entity entityA, Entity entityB) {
		return (int) Math.signum(cmTrans.get(entityA).position.z - cmTrans.get(entityB).position.z);
	}
}
