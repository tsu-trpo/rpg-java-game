package com.justgame.gamecomponentlogic.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.justgame.camera.Camera;
import com.justgame.gamecomponentlogic.PlayerInputController;
import com.justgame.gamecomponentlogic.PlayerStates;
import com.justgame.gamecomponentlogic.components.AnimationComponent;
import com.justgame.gamecomponentlogic.components.TransformComponent;

public class PlayerMovement extends EntitySystem {
	private PlayerInputController controller;
	private Camera camera;

	private TransformComponent transform;
	private AnimationComponent animation;
	private Vector2 newPosition = Vector2.Zero;
	private Vector2 saveDirection = Vector2.Zero;
	private Vector2 direction = Vector2.Zero;
	private float delta;

	public PlayerMovement(Entity player, PlayerInputController controller, Camera camera) {
		this.controller = controller;
		this.camera = camera;

		transform = player.getComponent(TransformComponent.class);
		animation = player.getComponent(AnimationComponent.class);
	}

	@Override
	public void update(float deltaTime) {
		if (!controller.isPlayerMoving()) {
			setPlayerStand();

			return;
		}

		saveDirection = direction.cpy();
		direction = controller.getDirection().cpy();

		setPlayerWalk();

		newPosition = new Vector2(transform.position.x, transform.position.y);
		delta = transform.velocity * deltaTime;

		newPosition.x += direction.x * delta;
		newPosition.y += direction.y * delta / 2.0f;

		camera.setCameraPosition(new Vector3(newPosition.x, newPosition.y, camera.position.z));

		transform.setPosition(newPosition);
	}

	private void setPlayerStand() {
		if (saveDirection.epsilonEquals(Directions.UP)) {
			animation.setState(PlayerStates.STAND_UP);
		} else if (saveDirection.epsilonEquals(Directions.UP_LEFT)) {
			animation.setState(PlayerStates.STAND_UP_LEFT);
		} else if (saveDirection.epsilonEquals(Directions.UP_RIGHT)) {
			animation.setState(PlayerStates.STAND_UP_RIGHT);
		} else if (saveDirection.epsilonEquals(Directions.DOWN)) {
			animation.setState(PlayerStates.STAND_DOWN);
		} else if (saveDirection.epsilonEquals(Directions.DOWN_LEFT)) {
			animation.setState(PlayerStates.STAND_DOWN_LEFT);
		} else if (saveDirection.epsilonEquals(Directions.DOWN_RIGHT)) {
			animation.setState(PlayerStates.STAND_DOWN_RIGHT);
		} else if (saveDirection.epsilonEquals(Directions.LEFT)) {
			animation.setState(PlayerStates.STAND_LEFT);
		} else if (saveDirection.epsilonEquals(Directions.RIGHT)) {
			animation.setState(PlayerStates.STAND_RIGHT);
		}
	}

	private void setPlayerWalk() {
		if (direction.epsilonEquals(Directions.UP)) {
			animation.setState(PlayerStates.WALK_UP);
		} else if (direction.epsilonEquals(Directions.UP_LEFT)) {
			animation.setState(PlayerStates.WALK_UP_LEFT);
		} else if (direction.epsilonEquals(Directions.UP_RIGHT)) {
			animation.setState(PlayerStates.WALK_UP_RIGHT);
		} else if (direction.epsilonEquals(Directions.DOWN)) {
			animation.setState(PlayerStates.WALK_DOWN);
		} else if (direction.epsilonEquals(Directions.DOWN_LEFT)) {
			animation.setState(PlayerStates.WALK_DOWN_LEFT);
		} else if (direction.epsilonEquals(Directions.DOWN_RIGHT)) {
			animation.setState(PlayerStates.WALK_DOWN_RIGHT);
		} else if (direction.epsilonEquals(Directions.LEFT)) {
			animation.setState(PlayerStates.WALK_LEFT);
		} else if (direction.epsilonEquals(Directions.RIGHT)) {
			animation.setState(PlayerStates.WALK_RIGHT);
		} else {
			setPlayerStand();
		}
	}

	static private class Directions {
		static private final Vector2 UP = new Vector2(0.0f, 1.0f);
		static private final Vector2 UP_LEFT = new Vector2(-1.0f, 1.0f);
		static private final Vector2 UP_RIGHT = new Vector2(1.0f, 1.0f);
		static private final Vector2 DOWN = new Vector2(0.0f, -1.0f);
		static private final Vector2 DOWN_LEFT = new Vector2(-1.0f, -1.0f);
		static private final Vector2 DOWN_RIGHT = new Vector2(1.0f, -1.0f);
		static private final Vector2 LEFT = new Vector2(-1.0f, 0.0f);
		static private final Vector2 RIGHT = new Vector2(1.0f, 0.0f);
	}
}
