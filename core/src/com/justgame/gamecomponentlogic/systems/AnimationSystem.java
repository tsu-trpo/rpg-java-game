package com.justgame.gamecomponentlogic.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.justgame.gamecomponentlogic.components.AnimationComponent;
import com.justgame.gamecomponentlogic.components.TextureComponent;

public class AnimationSystem extends IteratingSystem {
	private ComponentMapper<TextureComponent> textureMapper;
	private ComponentMapper<AnimationComponent> animationMapper;

	public AnimationSystem() {
		super(Family.all(TextureComponent.class,
		                 AnimationComponent.class).get());

		textureMapper = ComponentMapper.getFor(TextureComponent.class);
		animationMapper = ComponentMapper.getFor(AnimationComponent.class);
	}

	@Override
	protected void processEntity(Entity entity, float deltaTime) {
		AnimationComponent animation = animationMapper.get(entity);

		if (animation.animations.containsKey(animation.getState())) {
			TextureComponent texture = textureMapper.get(entity);
			texture.region = (TextureRegion) animation.animations.get(animation.getState())
					.getKeyFrame(animation.time, animation.isLooping);
		}

		animation.time += deltaTime;
	}
}
