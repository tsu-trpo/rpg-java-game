package com.justgame.gamecomponentlogic;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;

public class PlayerInputController implements InputProcessor {
	private Vector2 direction = Vector2.Zero;

	@Override
	public boolean keyDown(int keycode) {
		switch (keycode) {
			case Input.Keys.A:
				direction.x += -1.0f;
				break;

			case Input.Keys.D:
				direction.x += 1.0f;
				break;

			case Input.Keys.W:
				direction.y += 1.0f;
				break;

			case Input.Keys.S:
				direction.y += -1.0f;
				break;

			default:
				return false;
		}

		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		switch (keycode) {
			case Input.Keys.A:
				direction.x -= -1.0f;
				break;

			case Input.Keys.D:
				direction.x -= 1.0f;
				break;

			case Input.Keys.W:
				direction.y -= 1.0f;
				break;

			case Input.Keys.S:
				direction.y -= -1.0f;
				break;

			default:
				return false;
		}

		return true;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	public Vector2 getDirection() {
		return direction;
	}

	public boolean isPlayerMoving() {
		return Gdx.input.isKeyPressed(Input.Keys.W) ||
				Gdx.input.isKeyPressed(Input.Keys.A) ||
				Gdx.input.isKeyPressed(Input.Keys.S) ||
				Gdx.input.isKeyPressed(Input.Keys.D);
	}
}
