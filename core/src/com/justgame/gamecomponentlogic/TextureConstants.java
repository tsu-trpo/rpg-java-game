package com.justgame.gamecomponentlogic;

public class TextureConstants {
	public class Farmer {
		public static final String STAND_UP = "texture/Farmer/Idle/2_Farmer_Idle_strip4.png";
		static public final String STAND_UP_LEFT = "texture/Farmer/Idle/3_Farmer_Idle_strip4.png";
		static public final String STAND_UP_RIGHT = "texture/Farmer/Idle/1_Farmer_Idle_strip4.png";
		static public final String STAND_DOWN = "texture/Farmer/Idle/6_Farmer_Idle_strip4.png";
		static public final String STAND_DOWN_LEFT = "texture/Farmer/Idle/5_Farmer_Idle_strip4.png";
		static public final String STAND_DOWN_RIGHT = "texture/Farmer/Idle/7_Farmer_Idle_strip4.png";
		static public final String STAND_LEFT = "texture/Farmer/Idle/4_Farmer_Idle_strip4.png";
		static public final String STAND_RIGHT = "texture/Farmer/Idle/0_Farmer_Idle_strip4.png";

		static public final String WALK_UP = "texture/Farmer/Walk/2_Farmer_Walk_strip15.png";
		static public final String WALK_UP_LEFT = "texture/Farmer/Walk/3_Farmer_Walk_strip15.png";
		static public final String WALK_UP_RIGHT = "texture/Farmer/Walk/1_Farmer_Walk_strip15.png";
		static public final String WALK_DOWN = "texture/Farmer/Walk/6_Farmer_Walk_strip15.png";
		static public final String WALK_DOWN_LEFT = "texture/Farmer/Walk/5_Farmer_Walk_strip15.png";
		static public final String WALK_DOWN_RIGHT = "texture/Farmer/Walk/7_Farmer_Walk_strip15.png";
		static public final String WALK_LEFT = "texture/Farmer/Walk/4_Farmer_Walk_strip15.png";
		static public final String WALK_RIGHT = "texture/Farmer/Walk/0_Farmer_Walk_strip15.png";
	}
}
