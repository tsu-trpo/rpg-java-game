package com.justgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class AppPreferences {
	private static final String MUSIC_VOLUME = "music.volume";
	private static final String MUSIC_ENABLED = "music.enabled";
	private static final String SOUND_VOLUME = "sound.volume";
	private static final String SOUND_ENABLED = "sound.enabled";

	protected Preferences getPrefs() {
		final String name = "main.prefs";
		return Gdx.app.getPreferences(name);
	}

	public boolean isSoundEffectsEnabled() {
		return getPrefs().getBoolean(SOUND_ENABLED, true);
	}

	public void setSoundEffectsEnabled(boolean soundEffectsEnabled) {
		getPrefs().putBoolean(SOUND_ENABLED, soundEffectsEnabled);
		getPrefs().flush();
	}

	public boolean isMusicEnabled() {
		return getPrefs().getBoolean(MUSIC_ENABLED, true);
	}

	public void setMusicEnabled(boolean musicEnabled) {
		getPrefs().putBoolean(MUSIC_ENABLED, musicEnabled);
		getPrefs().flush();
	}

	public float getMusicVolume() {
		return getPrefs().getFloat(MUSIC_VOLUME, 0.5f);
	}

	public void setMusicVolume(float volume) {
		getPrefs().putFloat(MUSIC_VOLUME, volume);
		getPrefs().flush();
	}

	public float getSoundVolume() {
		return getPrefs().getFloat(SOUND_VOLUME, 0.5f);
	}

	public void setSoundVolume(float volume) {
		getPrefs().putFloat(SOUND_VOLUME, volume);
		getPrefs().flush();
	}
}
